﻿using System;
using System.IO;
using System.Text;
namespace base64_ver2
{
    public class base64Encoder
    {
        

        private bool _isDebug=false;


        public void setDebugStatus(bool isDebug)
        {
            this._isDebug = isDebug;   
        }

        public bool getDebugStatus()
        {
            return this._isDebug;
        }

        private char[] lookupTable = new char[64] {
        'A','B','C','D','E','F','G','H','I','J','K','L','M',
        'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'a','b','c','d','e','f','g','h','i','j','k','l','m',
        'n','o','p','q','r','s','t','u','v','w','x','y','z',
        '0','1','2','3','4','5','6','7','8','9','+','/'
                };

            public string encode(string theStringUWouldLikeToEncode)
            {
                //byte[] myBytes = { 155, 162, 233 ,44};
            string myRawString = theStringUWouldLikeToEncode;
                byte[] myBytes = Encoding.Default.GetBytes(myRawString);
            if(_isDebug) Console.WriteLine(myRawString);
                int myBytesLength = myBytes.Length;
                int total6BitLoop = ((myBytes.Length * 8) / 6);
                int totalBits = myBytes.Length * 8;
                int howManyByteStillICanProc = 0;
                int extraEqualSign = totalBits % 3;


                int tempTotalBits = totalBits;


                for (; ; )
                {
                    if (tempTotalBits % 6 != 0) tempTotalBits++;
                    else break;
                }


                byte[] my6BittedBytes = new byte[tempTotalBits / 6];


                int j = 0;
                for (int i = 0; i < myBytesLength; i += 3)
                {



                    howManyByteStillICanProc = (myBytesLength) - i;




                    int first6 = 0;
                    int second6 = 0;
                    int third6 = 0;
                    int last6 = 0;

                    byte firstByte = 0;
                    byte middleByte = 0;
                    byte lastByte = 0;



                    if (howManyByteStillICanProc >= 1) firstByte = myBytes[i];
                    if (howManyByteStillICanProc >= 2) middleByte = myBytes[i + 1];
                    if (howManyByteStillICanProc >= 3) lastByte = myBytes[i + 2];



                if(_isDebug) Console.WriteLine(firstByte + " " + middleByte + " " + lastByte + " --- LEFT CHARS(BYTES) => " + howManyByteStillICanProc);



                    if (howManyByteStillICanProc >= 1)
                    {
                        //FIRST 6
                        first6 = (byte)firstByte >> 2;
                        my6BittedBytes[j] = (byte)first6;
                        //FIRST 6

                    }

                    if (howManyByteStillICanProc >= 1)
                    {
                        //SECOND 6
                        int tempFirstByte = firstByte << 6;
                        tempFirstByte = (byte)tempFirstByte >> 2;
                        second6 = tempFirstByte | ((byte)middleByte >> 4);


                        my6BittedBytes[j + 1] = (byte)second6;

                        //SECOND 6

                    }

                    if (howManyByteStillICanProc >= 2)
                    {
                        //THIRD 6
                        int tempMiddleByte = middleByte << 4;
                        tempMiddleByte = (byte)tempMiddleByte >> 2;
                        int tempLastByte = lastByte >> 6;

                        third6 = (byte)tempMiddleByte | (byte)tempLastByte;


                        my6BittedBytes[j + 2] = (byte)third6;
                        //THIRD 6
                    }

                    if (howManyByteStillICanProc >= 3)
                    {
                        //LAST 6

                        int tempLastByteForLast = (byte)lastByte << 2;
                        tempLastByteForLast = (byte)tempLastByteForLast >> 2;
                        last6 = (byte)tempLastByteForLast;

                        my6BittedBytes[j + 3] = (byte)last6;

                        //LAST 6
                    }



                    //Console.WriteLine("How Many Byte I Can Proc? => " + howManyByteStillICanProc);


                    j += 4; //6bit bytes
                if(_isDebug) Console.WriteLine(first6 + " " + second6 + " " + third6 + " " + last6);





                }

            if(_isDebug)
            {
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine(" -- LISTING COLLECTED BYTES ---");

            }

                


                string base64Output = "";

                //Console.WriteLine(base64output);
                foreach (byte my6BitByte in my6BittedBytes)
                {
                    //Console.WriteLine((byte)my6BitByte);
                    base64Output += lookupTable[my6BitByte];

                }

                for (int k = 0; k < extraEqualSign; k++)
                {
                    base64Output += "=";
                }

            if(_isDebug)
            {
                Console.WriteLine("extra equal sign (=) " + extraEqualSign);
                Console.WriteLine(base64Output);

            }
                return base64Output;

            }


    }
}
